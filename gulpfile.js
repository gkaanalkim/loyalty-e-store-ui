var gulp = require('gulp'),
    connect = require('gulp-connect'),
    concat = require('gulp-concat'),
    sourcemaps = require('gulp-sourcemaps'),
    argv = require('yargs').argv;

if (argv.env === "dev") {
    gulp.task('connect', function () {
        connect.server({
            root: "./",
            port: 1234
        });
    });

    gulp.task('default', gulp.series(['connect']));
}
else if (argv.env === "preprod") {
    gulp.task('concat', function () {
        return gulp.src([
            'bower_components/navigo/lib/navigo.js',
            'bower_components/cryptojslib/components/core-min.js',
            'bower_components/cryptojslib/components/sha256-min.js',
            'bower_components/js-cookie/src/js.cookie.js',
            'app/lib/**/*.js',
            'app/service/**/*.js',
            'app/component/**/*.js',
            'app/modules/**/*.js',
            'app/App.js'
        ])
            .pipe(sourcemaps.init())
            .pipe(concat('all.js'))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest('preprod/js'))
    });

    gulp.task('connect', function () {
        connect.server({
            root: "./preprod",
            port: 1235
        });
    });

    gulp.task('default', gulp.series('concat', 'connect'));
}
else if (argv.env === "prod") {
    gulp.task('concat', function () {
        return gulp.src([
            'bower_components/navigo/lib/navigo.js',
            'bower_components/cryptojslib/components/core-min.js',
            'bower_components/cryptojslib/components/sha256-min.js',
            'bower_components/js-cookie/src/js.cookie.js',
            'app/lib/**/*.js',
            'app/service/**/*.js',
            'app/component/**/*.js',
            'app/modules/**/*.js',
            'app/App.js'
        ])
            .pipe(concat('all.js'))
            .pipe(gulp.dest('prod/js'))
    });

    gulp.task('default', gulp.series('concat'));
}