function AjaxRequest(contentType) {
    this.contentType = contentType
}

AjaxRequest.prototype.action = function (options) {
    var request = $.ajax({
        headers: {
            "Authorization": "Bearer " + Cookies.get("lst")
        },
        type: options.type,
        url: config.apiPath + options.url,
        data: options.data,
        contentType: this.contentType,
        beforeSend: options.beforeSend,
        complete: options.complete
    });

    request.fail(function (e) {
        //TODO global error handler
        if(e.status === 401) {
            router.navigate('/login');
        }
    });

    return request;
};
