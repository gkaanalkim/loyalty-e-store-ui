var FooterComponent = new Component({
    container: "footer",
    name: "App",
    render: function (categories) {
        var footerContainer = Component.createElement("div");

        var template = '<footer class="site-footer">\n' +
            '        <div class="container">\n' +
            '            <!-- Start Footer Info -->\n' +
            '            <div class="row">\n' +
            '                <!-- Start Contact Info -->\n' +
            '                <div class="col-lg-4 col-md-6">\n' +
            '                    <section class="widget widget-light-skin">\n' +
            '                        <h3 class="widget-title">KG BİLGİ İŞLEM TEKNOLOJİ A.Ş.</h3>\n' +
            '                        <p class="text-white"><i class="fa fa-phone"></i> +90 (216) 589 20 16</p>\n' +
            '                        <p class="text-white"><i class="fa fa-envelope-o"></i> info@kgteknoloji.com</p>\n' +
            '                        <p class="text-white"><i class="fa fa-map-marker"></i> Akasya Kent Kule A3 Blok 13. Kat Daire\n' +
            '                            No:25 Üsküdar / İstanbul</p>\n' +
            '                        <ul class="list-unstyled text-sm text-white">\n' +
            '                            <li><span class="opacity-50">Pzt. - Cum. : </span>09:00 - 18:00</li>\n' +
            '                        </ul>\n' +
            '                        <a class="social-button shape-circle sb-facebook sb-light-skin" href="#">\n' +
            '                            <i class="socicon-facebook"></i>\n' +
            '                        </a>\n' +
            '                        <a class="social-button shape-circle sb-twitter sb-light-skin" href="#">\n' +
            '                            <i class="socicon-twitter"></i>\n' +
            '                        </a>\n' +
            '                        <a class="social-button shape-circle sb-instagram sb-light-skin" href="#">\n' +
            '                            <i class="socicon-googleplus"></i>\n' +
            '                        </a>\n' +
            '                        <a class="social-button shape-circle sb-instagram sb-light-skin" href="#">\n' +
            '                            <i class="socicon-instagram"></i>\n' +
            '                        </a>\n' +
            '                    </section>\n' +
            '                </div>\n' +
            '                <!-- End Contact Info -->\n' +
            '                <!-- Start Mobile Apps -->\n' +
            '                <div class="col-lg-4 col-md-6">\n' +
            '                    <section class="widget widget-links widget-light-skin">\n' +
            '                        <h3 class="widget-title">HİZMETLERİMİZ</h3>\n' +
            '                        <ul>\n' +
            '                            <li><a href="#">Global Sadakat Uygulamaları</a></li>\n' +
            '                            <li><a href="#">Proje Yönetimi</a></li>\n' +
            '                            <li><a href="#">Operasyon Destek</a></li>\n' +
            '                            <li><a href="#">Kurumsal Mimari</a></li>\n' +
            '                            <li><a href="#">Sürüm Konfigrasyon Yönetimi</a></li>\n' +
            '                            <li><a href="#">Sistem Entegrasyonu</a></li>\n' +
            '                        </ul>\n' +
            '                    </section>\n' +
            '                </div>\n' +
            '                <!-- End Mobile Apps -->\n' +
            '                <!-- Start About Us -->\n' +
            '                <div class="col-lg-4 col-md-6">\n' +
            '                    <section class="widget widget-links widget-light-skin">\n' +
            '                        <h3 class="widget-title">HAKKIMIZDA</h3>\n' +
            '                        <ul>\n' +
            '                            <li><a href="#">Referanslar</a></li>\n' +
            '                            <li><a href="#">İK</a></li>\n' +
            '                            <li><a href="#">İletişim</a></li>\n' +
            '                        </ul>\n' +
            '                    </section>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            <!-- End Footer Info -->\n' +
            '            <hr class="hr-light">\n' +
            '            <!-- Start Copyright -->\n' +
            '            <p class="footer-copyright text-center">© 2018 KG Teknoloji | Tüm hakları saklıdır.</p>\n' +
            '            <!-- End Copyright -->\n' +
            '        </div>\n' +
            '    </footer>';

        footerContainer.innerHTML = template;

        return footerContainer;
    },
    actions: {
        querySelector: {
            'button': {
                click: function () {

                }
            }
        }
    }
});