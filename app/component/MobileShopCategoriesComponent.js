var MobileShopCategoriesComponent = new Component({
    container: "mobile-shop-categories",
    name: "mobile-shop-categories",
    render: function (categories) {
        //Data Operation
        var parentCategories = _.filter(categories, function (category) {
            return category.parentOid === null;
        });

        //HTML
        var shopCategoriesContainer = Component.createElement("div", {
            "class": "offcanvas-container",
            "id": "mobile-menu"
        });

        var accountLink = Component.createElement("div", {
            "href": "/#!",
            "class": "account-link"
        });

        var userAvatar = Component.createElement("div", {
            "class": "user-ava"
        });

        var userAvatarImg = Component.createElement("img", {
            "src": "app/template/images/account/user-ava-md.jpg"
        });

        var userInfo = Component.createElement("div", {
            "class": "user-info"
        });

        var userName = Component.createElement("h6", {
            "class": "user-name",
            "text": "Tony Stark"
        });

        var userBalance = Component.createElement("span", {
            "class": "text-sm text-white opacity-60",
            "text": "530 Ödül Puan"
        });

        var shopCategoriesMenuContainer = Component.createElement("nav", {
            "class": "offcanvas-menu"
        });

        var shopCategoriesMenu = Component.createElement("ul", {
            "class": "menu"
        });

        parentCategories.forEach(function (parentCategory, index) {
            var mainCategoryList = Component.createElement("li", {
                "class": index === 0 ? "has-children active" : "has-children"
            });

            var mainCategoryListTextContainer = Component.createElement("span");

            var mainCategoryListAnchor = Component.createElement("a", {
                "href": "/#!",
                "text": parentCategory.name
            });

            var subMenuToggle = Component.createElement("a", {
                "class": "sub-menu-toggle"
            });

            var subMenuContainer = Component.createElement("ul", {
                "class": "offcanvas-submenu"
            });

            mainCategoryListTextContainer.append(mainCategoryListAnchor);
            mainCategoryListTextContainer.append(subMenuToggle);
            mainCategoryList.append(mainCategoryListTextContainer);
            mainCategoryList.append(subMenuContainer);

            shopCategoriesMenu.append(mainCategoryList);

            var childCategories = _.filter(categories, function (category) {
                return category.parentOid === parentCategory.oid;
            });

            childCategories.forEach(function (childCategory) {
                var subMenuItem = Component.createElement("li");
                var subMenuItemText = Component.createElement("a", {
                    "href": "/#!/kategori/" + parentCategory.code + "/" + childCategory.code,
                    "text": childCategory.name
                });

                subMenuItem.append(subMenuItemText);
                subMenuContainer.append(subMenuItem);
            });
        });


        userInfo.append(userName);
        userInfo.append(userBalance);
        userAvatar.append(userAvatarImg);
        accountLink.append(userAvatar);
        accountLink.append(userInfo);

        shopCategoriesContainer.append(accountLink);
        shopCategoriesMenuContainer.append(shopCategoriesMenu);
        shopCategoriesContainer.append(shopCategoriesMenuContainer);

        return shopCategoriesContainer;
    }
});