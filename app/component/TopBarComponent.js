var TopBarComponent = new Component({
    container: "top-bar",
    name: "top-bar",
    render: function () {
        var topbar = Component.createElement("div", {
            "class": "topbar"
        });

        var template = '<div class="topbar-column">\n' +
            '        <a class="hidden-md-down" href="#"><i class="fa fa-phone"></i>&nbsp;+90 (216) 589 20 16</a>\n' +
            '        <a class="hidden-md-down" href="#"><i class="fa fa-envelope-o"></i>&nbsp;info@kgteknoloji.com</a>\n' +
            '        <a class="hidden-md-down" href="#"><i class="fa fa-map-marker"></i> Akasya Kent Kule A3 Blok 13. Kat Daire No:25 Üsküdar / İstanbul</a>\n' +
            '    </div>\n' +
            '    <div class="topbar-column">\n' +
            '        <div class="lang-currency-switcher-wrap">\n' +
            '            <div class="lang-currency-switcher dropdown-toggle">\n' +
            '                <span class="currency"><i class="fa fa-star"></i>&nbsp;RWP</span>\n' +
            '            </div>\n' +
            '            <div class="dropdown-menu">\n' +
            '                <div class="currency-select">\n' +
            '                    <select class="form-control form-control-rounded form-control-sm">\n' +
            '                        <option value="usd">$ USD</option>\n' +
            '                        <option value="usd">€ EUR</option>\n' +
            '                        <option value="usd">£ UKP</option>\n' +
            '                        <option value="usd">¥ JPY</option>\n' +
            '                    </select>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '        </div>\n' +
            '        <a class="social-button sb-facebook shape-none sb-dark soc-border" href="#" target="_blank"><i class="socicon-facebook"></i></a>\n' +
            '        <a class="social-button sb-twitter shape-none sb-dark" href="#" target="_blank"><i class="socicon-twitter"></i></a>\n' +
            '        <a class="social-button sb-instagram shape-none sb-dark" href="#" target="_blank"><i class="socicon-instagram"></i></a>\n' +
            '    </div>';

        topbar.innerHTML = template;

        return topbar;
    },
    actions: {
        querySelector: {
            'button': {
                click: function () {

                }
            }
        }
    }
});