var NavBarComponent = new Component({
    container: "nav-bar",
    name: "nav-bar",
    render: function (categories) {
        var navbar = Component.createElement("header", {
            "class": "navbar navbar-sticky"
        });

        var template = '<!-- Start Search -->\n' +
            '    <form class="site-search" method="get">\n' +
            '        <input type="text" name="site_search" placeholder="Type to search...">\n' +
            '        <div class="search-tools">\n' +
            '            <span class="clear-search">Clear</span>\n' +
            '            <span class="close-search"><i class="icon-cross"></i></span>\n' +
            '        </div>\n' +
            '    </form>\n' +
            '    <!-- End Search -->\n' +
            '    <!-- Start Logo -->\n' +
            '    <div class="site-branding">\n' +
            '        <div class="inner">\n' +
            '            <a class="offcanvas-toggle cats-toggle" href="#shop-categories" data-toggle="offcanvas"></a>\n' +
            '            <a class="offcanvas-toggle menu-toggle" href="#mobile-menu" data-toggle="offcanvas"></a>\n' +
            '            <a class="site-logo" href="index.html"><img src="app/template/images/logo/logo.png" alt="Inspina"></a>\n' +
            '        </div>\n' +
            '    </div>\n' +
            '    <div class="toolbar">\n' +
            '        <div class="inner">\n' +
            '            <div class="tools">\n' +
            '                <div class="search"><i class="icon-search"></i></div>\n' +
            '                <!-- Start Account -->\n' +
            '                <div class="account">\n' +
            '                    <a href="#"></a><i class="icon-head"></i>\n' +
            '                    <ul class="toolbar-dropdown">\n' +
            '                        <li class="sub-menu-user">\n' +
            '                            <div class="user-ava">\n' +
            '                                <img src="app/template/images/account/user-ava-sm.jpg" alt="Tony Stark">\n' +
            '                            </div>\n' +
            '                            <div class="user-info">\n' +
            '                                <h6 class="user-name">Tony Stark</h6>\n' +
            '                                <span class="text-xs text-muted">530 Reward Points</span>\n' +
            '                            </div>\n' +
            '                        </li>\n' +
            '                        <li><a href="account-profile.html">My Profile</a></li>\n' +
            '                        <li><a href="account-orders.html">My Orders</a></li>\n' +
            '                        <li><a href="account-wishlist.html">My Wishlist</a></li>\n' +
            '                        <li class="sub-menu-separator"></li>\n' +
            '                        <li><a href="#"><i class="fa fa-lock"></i> Sign Out</a></li>\n' +
            '                    </ul>\n' +
            '                </div>\n' +
            '                <!-- End Account -->\n' +
            '                <!-- Start Cart -->\n' +
            '                <div class="cart">\n' +
            '                    <a href="#"></a>\n' +
            '                    <i class="icon-bag"></i>\n' +
            '                    <span class="count">3</span>\n' +
            '                    <span class="subtotal">$1920</span>\n' +
            '                    <div class="toolbar-dropdown">\n' +
            '                        <div class="dropdown-product-item">\n' +
            '                            <span class="dropdown-product-remove"><i class="icon-cross"></i></span>\n' +
            '                            <a class="dropdown-product-thumb" href="shop-single-1.html">\n' +
            '                                <img src="app/template/images/cart-dropdown/01.jpg" alt="Product">\n' +
            '                            </a>\n' +
            '                            <div class="dropdown-product-info">\n' +
            '                                <a class="dropdown-product-title" href="shop-single-1.html">Samsung Galaxy A8</a>\n' +
            '                                <span class="dropdown-product-details">1 x $520</span>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                        <div class="dropdown-product-item">\n' +
            '                            <span class="dropdown-product-remove"><i class="icon-cross"></i></span>\n' +
            '                            <a class="dropdown-product-thumb" href="shop-single-2.html">\n' +
            '                                <img src="app/template/images/cart-dropdown/02.jpg" alt="Product">\n' +
            '                            </a>\n' +
            '                            <div class="dropdown-product-info">\n' +
            '                                <a class="dropdown-product-title" href="shop-single-2.html">Panasonic TX-32</a>\n' +
            '                                <span class="dropdown-product-details">2 x $400</span>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                        <div class="dropdown-product-item">\n' +
            '                            <span class="dropdown-product-remove"><i class="icon-cross"></i></span>\n' +
            '                            <a class="dropdown-product-thumb" href="shop-single-3.html">\n' +
            '                                <img src="app/template/images/cart-dropdown/03.jpg" alt="Product">\n' +
            '                            </a>\n' +
            '                            <div class="dropdown-product-info">\n' +
            '                                <a class="dropdown-product-title" href="shop-single-3.html">Acer Aspire 15.6 i3</a>\n' +
            '                                <span class="dropdown-product-details">1 x $600</span>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                        <div class="toolbar-dropdown-group">\n' +
            '                            <div class="column">\n' +
            '                                <span class="text-lg">Total:</span>\n' +
            '                            </div>\n' +
            '                            <div class="column text-right">\n' +
            '                                <span class="text-lg text-medium">$1920 </span>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                        <div class="toolbar-dropdown-group">\n' +
            '                            <div class="column">\n' +
            '                                <a class="btn btn-sm btn-block btn-secondary" href="cart.html">View Cart</a>\n' +
            '                            </div>\n' +
            '                            <div class="column">\n' +
            '                                <a class="btn btn-sm btn-block btn-success" href="checkout-address.html">Checkout</a>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '                <!-- End Cart -->\n' +
            '            </div>\n' +
            '        </div>\n' +
            '    </div>\n' +
            '    <!-- End Toolbar -->';

        navbar.innerHTML = template;

        return navbar;
    }
});