var LoginComponent = new Component({
    container: "login-form",
    name: "LoginForm",
    render: function () {
        var container = Component.createElement("div");

        container.innerHTML = '' +
            '<div class="login-box">\n' +
            '    <h4 class="margin-bottom-1x">Giriş Yap</h4>\n' +
            '    <div class="form-group input-group">\n' +
            '        <input class="form-control" type="text" placeholder="Kullanıcı adınız" id="txtUsername" required><span\n' +
            '            class="input-group-addon"><i class="icon-mail"></i></span>\n' +
            '    </div>\n' +
            '    <div class="form-group input-group">\n' +
            '        <input class="form-control" type="password" placeholder="Şifreniz" id="txtPassword" required><span\n' +
            '            class="input-group-addon"><i class="icon-lock"></i></span>\n' +
            '    </div>\n' +
            '    <div class="d-flex flex-wrap justify-content-between padding-bottom-1x">\n' +
            '        <div class="custom-control custom-checkbox">\n' +
            '            <input class="custom-control-input" type="checkbox" id="remember_me" checked>\n' +
            '            <label class="custom-control-label" for="remember_me">Beni hatırla</label>\n' +
            '        </div>\n' +
            '        <a class="navi-link" href="account-password-recovery.html">Şifremi unuttum</a>\n' +
            '    </div>\n' +
            '    <div class="text-center text-sm-right">\n' +
            '        <button class="btn btn-primary margin-bottom-none" type="submit">GİRİŞ YAP</button>\n' +
            '    </div>\n' +
            '</div>';

        return container;
    },
    actions: {
        querySelector: {
            'button': {
                click: function () {
                    var username = this.parentElement.parentElement.querySelector("#txtUsername").value;
                    var password = this.parentElement.parentElement.querySelector("#txtPassword").value;

                    AuthenticationService.login(username, password, function (res) {
                        if (res.token) {
                            Cookies.set('lst', res.token, {expires: 7});
                            router.navigate('/');
                        }
                    })
                }
            }
        }
    }
});