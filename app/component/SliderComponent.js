var SliderComponent = new Component({
    container: "slider",
    name: "App",
    render: function (categories) {
        var sliderContainer = Component.createElement("layout");

        var template = '<div class="hero-slider home-1-hero">\n' +
            '        <div class="owl-carousel large-controls dots-inside"\n' +
            '             data-owl-carousel=\'{"nav": true, "dots": true, "loop": true, "autoplay": true, "autoplayTimeou": 7000}\'>\n' +
            '            <!-- Start Slide #1 -->\n' +
            '            <div class="item">\n' +
            '                <div class="container padding-top-3x">\n' +
            '                    <div class="row justify-content-center align-items-center">\n' +
            '                        <div class="col-lg-5 col-md-6 padding-bottom-2x text-md-left text-center hidden-md-down">\n' +
            '                            <div class="from-bottom">\n' +
            '                                <img class="d-inline-block w-150 mb-4" src="app/template/images/hero-slider/logo02.png"\n' +
            '                                     alt="Puma">\n' +
            '                                <div class="h2 text-body text-normal mb-2 pt-1">Sony Mobile Collection</div>\n' +
            '                                <div class="h2 text-body text-normal mb-4 pb-1">starting at <span\n' +
            '                                        class="text-bold">$200</span></div>\n' +
            '                            </div>\n' +
            '                            <a class="btn btn-primary scale-up delay-1" href="shop-categories-1.html">Shop Now</a>\n' +
            '                        </div>\n' +
            '                        <div class="col-md-6 padding-bottom-2x mb-3">\n' +
            '                            <img class="d-block mx-auto" src="app/template/images/hero-slider/02.png" alt="Puma Backpack">\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            <!-- End Slide #1 -->\n' +
            '            <!-- Start Slide #2 -->\n' +
            '            <div class="item">\n' +
            '                <div class="container padding-top-3x">\n' +
            '                    <div class="row justify-content-center align-items-center">\n' +
            '                        <div class="col-lg-5 col-md-6 padding-bottom-2x text-md-left text-center hidden-md-down">\n' +
            '                            <div class="from-bottom">\n' +
            '                                <img class="d-inline-block w-200 mb-4" src="app/template/images/hero-slider/logo01.png"\n' +
            '                                     alt="Converse">\n' +
            '                                <div class="h2 text-body text-normal mb-2 pt-1">Intel Laptops Collection</div>\n' +
            '                                <div class="h2 text-body text-normal mb-4 pb-1">starting at <span\n' +
            '                                        class="text-bold">$372</span></div>\n' +
            '                            </div>\n' +
            '                            <a class="btn btn-primary scale-up delay-1" href="shop-categories-2.html">Shop Now</a>\n' +
            '                        </div>\n' +
            '                        <div class="col-md-6 padding-bottom-2x mb-3">\n' +
            '                            <img class="d-block mx-auto" src="app/template/images/hero-slider/01.png"\n' +
            '                                 alt="Chuck Taylor All Star II">\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            <!-- End Slide #2 -->\n' +
            '            <!-- Start Slide #3 -->\n' +
            '            <div class="item">\n' +
            '                <div class="container padding-top-3x">\n' +
            '                    <div class="row justify-content-center align-items-center">\n' +
            '                        <div class="col-lg-5 col-md-6 padding-bottom-2x text-md-left text-center hidden-md-down">\n' +
            '                            <div class="from-bottom">\n' +
            '                                <img class="d-inline-block mb-4 home-1-hero-item"\n' +
            '                                     src="app/template/images/hero-slider/logo03.png" alt="Motorola">\n' +
            '                                <div class="h2 text-body text-normal mb-2 pt-1">LG Televisions Collection</div>\n' +
            '                                <div class="h2 text-body text-normal mb-4 pb-1">starting at <span\n' +
            '                                        class="text-bold">$460</span></div>\n' +
            '                            </div>\n' +
            '                            <a class="btn btn-primary scale-up delay-1" href="shop-categories-3.html">Shop Now</a>\n' +
            '                        </div>\n' +
            '                        <div class="col-md-6 padding-bottom-2x mb-3">\n' +
            '                            <img class="d-block mx-auto" src="app/template/images/hero-slider/03.png" alt="Moto 360">\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            <!-- End Slide #3 -->\n' +
            '        </div>\n' +
            '    </div>';

        sliderContainer.innerHTML = template;

        return sliderContainer;
    }
});