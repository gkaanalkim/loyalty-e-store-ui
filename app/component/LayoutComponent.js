var LayoutComponent = new Component({
    container: "app",
    name: "App",
    render: function (categories) {
        var layoutContainer = Component.createElement("layout");

        var shopCategories = Component.createElement("shop-categories");
        var mobileMenu = Component.createElement("mobile-shop-categories");
        var topBar = Component.createElement("top-bar");
        var navbar = Component.createElement("nav-bar");
        var content = Component.createElement("div", {
            "content-area": ""
        });
        var footer = Component.createElement("footer");

        layoutContainer.append(shopCategories);
        layoutContainer.append(mobileMenu);
        layoutContainer.append(topBar);
        layoutContainer.append(navbar);
        layoutContainer.append(content);
        layoutContainer.append(footer);

        return layoutContainer;
    },
    actions: {
        querySelector: {
            'button': {
                click: function () {

                }
            }
        }
    }
});