var ProductSlider = new Component({
    name: "App",
    render: function (state) {
        var topCategoriesComponentContainer = Component.createElement("layout");

        var template = ' <section class="container padding-top-3x padding-bottom-3x">\n' +
            '        <h3 class="text-center mb-30">' + state.pageTitle + '</h3>\n' +
            '        <div class="owl-carousel"\n' +
            '             data-owl-carousel=\'{"nav": false, "dots": false, "margin": 30, "responsive": {"0":{"items":1},"576":{"items":2},"768":{"items":3},"991":{"items":4},"1200":{"items":4}} }\'>\n' +
            '            <!-- Start Product #1 -->\n' +
            '            <div class="grid-item">\n' +
            '                <div class="product-card">\n' +
            '                    <a class="product-thumb" href="shop-single-3.html">\n' +
            '                        <img src="app/template/images/shop/products/01.jpg" alt="Product">\n' +
            '                    </a>\n' +
            '                    <h3 class="product-title"><a href="shop-single-3.html">iPhone X</a></h3>\n' +
            '                    <h4 class="product-price">$749.99</h4>\n' +
            '                    <div class="product-buttons">\n' +
            '                        <div class="product-buttons">\n' +
            '                            <button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip"\n' +
            '                                    title="Whishlist">\n' +
            '                                <i class="icon-heart"></i>\n' +
            '                            </button>\n' +
            '                            <button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success"\n' +
            '                                    data-toast-position="topRight" data-toast-icon="icon-circle-check"\n' +
            '                                    data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to\n' +
            '                                Cart\n' +
            '                            </button>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            <!-- End Product #1 -->\n' +
            '            <!-- Start Product #2 -->\n' +
            '            <div class="grid-item">\n' +
            '                <div class="product-card">\n' +
            '                    <div class="rating-stars">\n' +
            '                        <i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i\n' +
            '                            class="icon-star filled"></i><i class="icon-star filled"></i>\n' +
            '                    </div>\n' +
            '                    <a class="product-thumb" href="shop-single-2.html">\n' +
            '                        <img src="app/template/images/shop/products/05.jpg" alt="Product">\n' +
            '                    </a>\n' +
            '                    <h3 class="product-title"><a href="shop-single-2.html">Panasonic TX-32</a></h3>\n' +
            '                    <h4 class="product-price">$949.50</h4>\n' +
            '                    <div class="product-buttons">\n' +
            '                        <div class="product-buttons">\n' +
            '                            <button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip"\n' +
            '                                    title="Whishlist">\n' +
            '                                <i class="icon-heart"></i>\n' +
            '                            </button>\n' +
            '                            <button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success"\n' +
            '                                    data-toast-position="topRight" data-toast-icon="icon-circle-check"\n' +
            '                                    data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to\n' +
            '                                Cart\n' +
            '                            </button>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            <!-- End Product #2 -->\n' +
            '            <!-- Start Product #3 -->\n' +
            '            <div class="grid-item">\n' +
            '                <div class="product-card">\n' +
            '                    <a class="product-thumb" href="shop-single-3.html">\n' +
            '                        <img src="app/template/images/shop/products/09.jpg" alt="Product">\n' +
            '                    </a>\n' +
            '                    <h3 class="product-title"><a href="shop-single-3.html">Sony HDR-AS50R</a></h3>\n' +
            '                    <h4 class="product-price">$700.00</h4>\n' +
            '                    <div class="product-buttons">\n' +
            '                        <div class="product-buttons">\n' +
            '                            <button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip"\n' +
            '                                    title="Whishlist">\n' +
            '                                <i class="icon-heart"></i>\n' +
            '                            </button>\n' +
            '                            <button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success"\n' +
            '                                    data-toast-position="topRight" data-toast-icon="icon-circle-check"\n' +
            '                                    data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to\n' +
            '                                Cart\n' +
            '                            </button>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            <!-- End Product #3 -->\n' +
            '            <!-- Start Product #4 -->\n' +
            '            <div class="grid-item">\n' +
            '                <div class="product-card">\n' +
            '                    <div class="rating-stars">\n' +
            '                        <i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i\n' +
            '                            class="icon-star filled"></i><i class="icon-star filled"></i>\n' +
            '                    </div>\n' +
            '                    <a class="product-thumb" href="shop-single-2.html">\n' +
            '                        <img src="app/template/images/shop/products/13.jpg" alt="Product">\n' +
            '                    </a>\n' +
            '                    <h3 class="product-title"><a href="shop-single-2.html">HP LaserJet Pro 200</a></h3>\n' +
            '                    <h4 class="product-price">$249.50</h4>\n' +
            '                    <div class="product-buttons">\n' +
            '                        <div class="product-buttons">\n' +
            '                            <button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip"\n' +
            '                                    title="Whishlist">\n' +
            '                                <i class="icon-heart"></i>\n' +
            '                            </button>\n' +
            '                            <button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success"\n' +
            '                                    data-toast-position="topRight" data-toast-icon="icon-circle-check"\n' +
            '                                    data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to\n' +
            '                                Cart\n' +
            '                            </button>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            <!-- End Product #4 -->\n' +
            '            <!-- Start Product #5 -->\n' +
            '            <div class="grid-item">\n' +
            '                <div class="product-card">\n' +
            '                    <a class="product-thumb" href="shop-single-3.html">\n' +
            '                        <img src="app/template/images/shop/products/17.jpg" alt="Product">\n' +
            '                    </a>\n' +
            '                    <h3 class="product-title"><a href="shop-single-3.html">Apple Watch 3</a></h3>\n' +
            '                    <h4 class="product-price">$449.00</h4>\n' +
            '                    <div class="product-buttons">\n' +
            '                        <div class="product-buttons">\n' +
            '                            <button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip"\n' +
            '                                    title="Whishlist">\n' +
            '                                <i class="icon-heart"></i>\n' +
            '                            </button>\n' +
            '                            <button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success"\n' +
            '                                    data-toast-position="topRight" data-toast-icon="icon-circle-check"\n' +
            '                                    data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to\n' +
            '                                Cart\n' +
            '                            </button>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            <!-- End Product #5 -->\n' +
            '            <!-- Start Product #6 -->\n' +
            '            <div class="grid-item">\n' +
            '                <div class="product-card">\n' +
            '                    <div class="rating-stars">\n' +
            '                        <i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i\n' +
            '                            class="icon-star filled"></i><i class="icon-star filled"></i>\n' +
            '                    </div>\n' +
            '                    <a class="product-thumb" href="shop-single-2.html">\n' +
            '                        <img src="app/template/images/shop/products/21.jpg" alt="Product">\n' +
            '                    </a>\n' +
            '                    <h3 class="product-title"><a href="shop-single-2.html">Acer Aspire 15.6 i3</a></h3>\n' +
            '                    <h4 class="product-price">$649.50</h4>\n' +
            '                    <div class="product-buttons">\n' +
            '                        <div class="product-buttons">\n' +
            '                            <button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip"\n' +
            '                                    title="Whishlist">\n' +
            '                                <i class="icon-heart"></i>\n' +
            '                            </button>\n' +
            '                            <button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success"\n' +
            '                                    data-toast-position="topRight" data-toast-icon="icon-circle-check"\n' +
            '                                    data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to\n' +
            '                                Cart\n' +
            '                            </button>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            <!-- End Product #6 -->\n' +
            '        </div>\n' +
            '    </section>';

        topCategoriesComponentContainer.innerHTML = template;

        return topCategoriesComponentContainer;
    }
});