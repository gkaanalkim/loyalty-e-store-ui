var CategoryProductComponent = new Component({
    container: "category-product",
    name: "Category",
    render: function (products) {
        console.log(products);

        var productContainer = Component.createElement("div");

        products.forEach(function (product) {
            var gridItem = Component.createElement("div", {class: "grid-item"});

            var productCard = Component.createElement("div", {class: "product-card"});

            var productThumb = Component.createElement("a", {href: "/#!/product/" + product.sku, class: "product-thumb"});
            var productImg = Component.createElement("img", {src: "app/template/images/shop/products/01.jpg"});

            var productTitle = Component.createElement("h3", {class: "product-title"});
            var productTitleLink = Component.createElement("a", {href: "/#!/product/" + product.sku, text: product.name});


            productTitle.append(productTitleLink);

            productThumb.append(productImg);

            var productPrice = Component.createElement("h4", {class: "product-price", text: product.price});

            var productButtons = Component.createElement("div", {class: "product-buttons"});

            var btnWishList = Component.createElement("button", {
                class: "btn btn-outline-secondary btn-sm btn-wishlist",
                "data-toggle": "tooltip",
                "title": "Whishlist"
            });

            var btnWishListIcon = Component.createElement("i", {
                class: "icon-heart"
            });

            var btnAddToCart = Component.createElement("button", {
                class: "btn btn-outline-primary btn-sm",
                text: "Sepete Ekle"
            });

            btnWishList.append(btnWishListIcon);
            productButtons.append(btnWishList);
            productButtons.append(btnAddToCart);

            productCard.append(productThumb);
            productCard.append(productTitle);
            productCard.append(productPrice);
            productCard.append(productButtons);

            gridItem.append(productCard);

            productContainer.append(gridItem);
        });

        return productContainer;
    },
    actions: {
        querySelector: {
            'button': {
                click: function () {

                }
            }
        }
    }
});