var ShopCategoriesComponent = new Component({
    container: "shop-categories",
    name: "shop-categories",
    render: function (categories) {
        //Data Operation
        var parentCategories = _.filter(categories, function (category) {
            return category.parentOid === null;
        });

        //HTML
        var shopCategoriesContainer = Component.createElement("div", {
            "class": "offcanvas-container",
            "id": "shop-categories"
        });

        var shopCategoriesHeader = Component.createElement("div", {
            "class": "offcanvas-header"
        });

        var shopCategoriesTitle = Component.createElement("h3", {
            "class": "offcanvas-title",
            "text": "Alışveriş Kategorileri"
        });

        var shopCategoriesMenuContainer = Component.createElement("nav", {
            "class": "offcanvas-menu"
        });

        var shopCategoriesMenu = Component.createElement("ul", {
            "class": "menu"
        });

        parentCategories.forEach(function (parentCategory) {
            var mainCategoryList = Component.createElement("li", {
                "class": "has-children"
            });

            var mainCategoryListTextContainer = Component.createElement("span");

            var mainCategoryListAnchor = Component.createElement("a", {
                "href": "/#!",
                "text": parentCategory.name
            });

            var subMenuToggle = Component.createElement("a", {
                "class": "sub-menu-toggle"
            });

            var subMenuContainer = Component.createElement("ul", {
                "class": "offcanvas-submenu"
            });

            mainCategoryListTextContainer.append(mainCategoryListAnchor);
            mainCategoryListTextContainer.append(subMenuToggle);
            mainCategoryList.append(mainCategoryListTextContainer);
            mainCategoryList.append(subMenuContainer);

            shopCategoriesMenu.append(mainCategoryList);

            var childCategories = _.filter(categories, function (category) {
                return category.parentOid === parentCategory.oid;
            });

            childCategories.forEach(function (childCategory) {
                var subMenuItem = Component.createElement("li");
                var subMenuItemText = Component.createElement("a", {
                    "href": "/#!/kategori/" + parentCategory.code + "/" + childCategory.code,
                    "text": childCategory.name
                });

                subMenuItem.append(subMenuItemText);
                subMenuContainer.append(subMenuItem);
            });
        });


        shopCategoriesHeader.append(shopCategoriesTitle);
        shopCategoriesContainer.append(shopCategoriesHeader);
        shopCategoriesMenuContainer.append(shopCategoriesMenu);
        shopCategoriesContainer.append(shopCategoriesMenuContainer);

        return shopCategoriesContainer;
    }
});