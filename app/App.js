var config = null;
var util = new Util();

var configurationRequest = util.getConfiguration();
var request = new AjaxRequest("application/json");
var router = "";

configurationRequest.done(function (res) {
    config = new Config(res.apiPath, res.router.rootPath, res.router.useHash, res.router.hash);
    router = new Navigo(config.rootPath, config.useHash, config.hash);

    if (!location.href.includes('login') && !location.href.endsWith("#!/")) {
        LayoutComponent.render();
        CategoryService.readAll(function (categories) {
            ShopCategoriesComponent.setState(categories).render();
            MobileShopCategoriesComponent.setState(categories).render();
            TopBarComponent.render();
            NavBarComponent.render();
            FooterComponent.render();

            $("head").append("<script src='app/template/js/script.js'></script>"); // todo fixed
        });
    }

    var routingPaths = {
        'login': function () {
            util.loadPage({
                appendTo: "app",
                path: "/app/pages/login/login.html",
                dependencies: {
                    LoginComponent: LoginComponent
                },
                success: function (template, dependencies) {
                    dependencies.LoginComponent.render();
                },
                error: onRouterError
            });
        },
        'kategori/:parentCode': function (params) {
            util.loadPage({
                path: "/app/pages/category-detail/category-detail.html",
                dependencies: {
                    CategoryFilterComponent: CategoryFilterComponent,
                    CategoryProductComponent: CategoryProductComponent,
                    CategoryService: CategoryService
                },
                success: function (template, dependencies) {
                    dependencies.CategoryService.readFilters(params.parentCode, function (categoryFilters) {
                        dependencies.CategoryFilterComponent.setState(categoryFilters).render();
                    });
                },
                error: onRouterError
            });
        },
        'kategori/:parentCode/:childCode': function (params) {
            util.loadPage({
                path: "/app/pages/category-detail/category-detail.html",
                dependencies: {
                    CategoryFilterComponent: CategoryFilterComponent,
                    CategoryProductComponent: CategoryProductComponent,
                    CategoryService: CategoryService
                },
                success: function (template, dependencies) {
                    dependencies.CategoryService.readFilters(params.childCode, function (categoryFilters) {
                        dependencies.CategoryFilterComponent.setState(categoryFilters).render();
                    });

                    dependencies.CategoryService.readProduct(params.childCode, 0, 12, function (categoryProducts) {
                        dependencies.CategoryProductComponent.setState(categoryProducts).render();
                        $("head").append("<script src='app/template/js/script.js'></script>"); // todo fixed
                    });
                },
                error: onRouterError
            });
        },
        'product/:sku': function (params) {
            util.loadPage({
                path: "/app/pages/product-detail/product-detail.html",
                dependencies: {
                    ProductDetailComponent: ProductDetailComponent,
                    ProductService: ProductService
                },
                success: function (template, dependencies) {
                    ProductService.readProductDetailBySku(params.sku, function (product) {
                        ProductDetailComponent.setState(product).render();
                    });
                },
                error: onRouterError
            });
        }
    };

    router.on(routingPaths);

    // Homepage
    router.on(function () {
        LayoutComponent.render();
        CategoryService.readAll(function (categories) {
            ShopCategoriesComponent.setState(categories).render();
            MobileShopCategoriesComponent.setState(categories).render();
            TopBarComponent.render();
            NavBarComponent.render();
            FooterComponent.render();
        });
        util.loadPage({
            path: "/app/pages/home/home.html",
            dependencies: {},
            success: function (template, dependencies) {
                SliderComponent.render();
                ProductSlider.setContainer("newest-product").setState({
                    pageTitle: "En Yeniler"
                }).render();

                ProductSlider.setContainer("lowest-price-product").setState({
                    pageTitle: "En Düşük Fiyatlar"
                }).render();

                ProductSlider.setContainer("highest-price-product").setState({
                    pageTitle: "En Yüksek Fiyatlar"
                }).render();

                $("head").append("<script src='app/template/js/owl.carousel.min.js'></script>"); // todo fixed
            },
            error: onRouterError
        });
    });

    // 404 Page
    router.notFound(function () {
        //TODO 404 Not Found
        util.loadPage({
            path: "/app/pages/404/404.html",
            success: function (res) {
                console.log("This is 404 page!");
            }
        });
    });

    router.resolve();
});

configurationRequest.fail(function (res) {
    //TODO error handler
});

function onRouterError(e) {
    //TODO error handling
    console.log("Failed to load page", e.statusText);
}