var Config = (function () {
    var apiPath;
    var rootPath;
    var useHash;
    var hash;

    function Config(apiPath, rootPath, useHash, hash) {
        this.apiPath = apiPath;
        this.rootPath = rootPath;
        this.useHash = useHash;
        this.hash = hash;
    }

    return Config;
})();