function Util() {
}

Util.prototype.getConfiguration = function () {
    var ajaxCallback = $.ajax({
        url: "../app/config.json",
        type: "GET",
        dataType: "json"
    });

    return ajaxCallback;
};

Util.prototype.loadPage = function (options) {//pageName, container
    options.appendTo = options.appendTo || "[content-area]";
    options.dependencies = options.dependencies || {};

    var request = $.ajax({
        url: options.path,
        type: "GET"
    });

    request.done(function (res) {
        document.querySelector(options.appendTo).innerHTML = res;
        options.success.call(this, res, options.dependencies)
    });

    if(options.error) {
        request.fail(options.error);
    } else {
        request.fail(function () {
            //TODO error handling
            console.log("Failed to load page", e);
        });
    }

    return request;
};