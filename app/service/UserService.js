function UserService() {

}

UserService.whoAmI = function (successHandler) {
    var whoAmIRequest = request.action({
        type: "GET",
        url: "user/whoAmI",
        contentType: "application/json"
    });

    whoAmIRequest.done(successHandler);
};