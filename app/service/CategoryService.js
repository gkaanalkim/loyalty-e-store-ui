function CategoryService() {

}

CategoryService.readAll = function (successHandler) {
    var categoryRequest = new AjaxRequest().action({
        type: "GET",
        url: "category",
        contentType: "application/json"
    });

    categoryRequest.done(successHandler);
};

CategoryService.readProduct = function (categoryCode, from, count, successHandler) {
    var readProductRequest = new AjaxRequest().action({
        type: "GET",
        url: "category/" + categoryCode + "/product/from/" + from + "/count/" + count,
        contentType: "application/json"
    });

    readProductRequest.done(successHandler);
};

CategoryService.readFilters = function (categoryCode, successHandler) {
    var readProductRequest = new AjaxRequest().action({
        type: "GET",
        url: "category/" + categoryCode + "/filter",
        contentType: "application/json"
    });

    readProductRequest.done(successHandler);
};