function ProductService() {

}

ProductService.readProduct = function (oid, successHandler) {
    var readProductRequest = request.action({
        type: "GET",
        url: "product/",
        contentType: "application/json"
    });

    readProductRequest.done(successHandler);
};

ProductService.readProductAttribute = function (oid, successHandler) {
    var readProductAttributeRequest = request.action({
        type: "GET",
        url: "product/" + oid + "/attribute",
        contentType: "application/json"
    });

    readProductAttributeRequest.done(successHandler);
};

ProductService.readProductVariation = function (oid, successHandler) {
    var readProductDetailRequest = request.action({
        type: "GET",
        url: "product/" + oid + "/variation",
        contentType: "application/json"
    });

    readProductDetailRequest.done(successHandler);
};

ProductService.readProductDetail = function (oid, successHandler) {
    var readProductDetailRequest = request.action({
        type: "GET",
        url: "product/" + oid + "/detail",
        contentType: "application/json"
    });

    readProductDetailRequest.done(successHandler);
};

ProductService.readProductDetailBySku = function (sku, successHandler) {
    var readProductDetailRequest = request.action({
        type: "GET",
        url: "product/sku/" + sku ,
        contentType: "application/json"
    });

    readProductDetailRequest.done(successHandler);
};

ProductService.readByFilters = function (filterMap) {
    var queryParams = "?";

    for (var i in filterMap) {
        if (filterMap.hasOwnProperty(i)) {
            var filter = i;
            var value = filterMap[i];

            queryParams += filter + "=" + value + "&";
        }
    }

    var readByFiltersRequest = request.action({
        type: "GET",
        url: "product/filter/" + queryParams,
        contentType: "application/json"
    });

    readByFiltersRequest.done(successHandler);
};

ProductService.readProductByCategory = function (categoryId, from, count, successHandler) {
    var readProductByCategoryRequest = request.action({
        type: "GET",
        url: "product/category/" + categoryId + "/from/" + from + "/count/" + count,
        contentType: "application/json"
    });

    readProductByCategoryRequest.done(successHandler);
};

ProductService.readNewestByCategory = function (categoryId, from, count, successHandler) {
    var readNewestByCategoryRequest = request.action({
        type: "GET",
        url: "product/newest/category/" + categoryId + "/from/" + from + "/count/" + count,
        contentType: "application/json"
    });

    readNewestByCategoryRequest.done(successHandler);
};

ProductService.readLowestPriceByCategory = function (categoryId, from, count, successHandler) {
    var readLowestPriceByCategoryRequest = request.action({
        type: "GET",
        url: "product/lowestPrice/category/" + categoryId + "/from/" + from + "/count/" + count,
        contentType: "application/json"
    });

    readLowestPriceByCategoryRequest.done(successHandler);
};

ProductService.readHighestPriceByCategory = function (categoryId, from, count, successHandler) {
    var readHighestPriceByCategoryRequest = request.action({
        type: "GET",
        url: "product/highestPrice/category/" + categoryId + "/from/" + from + "/count/" + count,
        contentType: "application/json"
    });

    readHighestPriceByCategoryRequest.done(successHandler);
};