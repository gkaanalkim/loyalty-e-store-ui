function AuthenticationService() {
}

AuthenticationService.login = function (username, password, successHandler) {
    var login = request.action({
        type: "POST",
        url: "auth/login",
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({
            username: username,
            password: CryptoJS.SHA256(password).toString()
        })
    });

    login.done(successHandler);
};

AuthenticationService.logout = function (token, successHandler) {
    var logout = ajax.action({
        type: "POST",
        url: "auth/logout",
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({
            token: token
        })
    });

    logout.done(successHandler);
};